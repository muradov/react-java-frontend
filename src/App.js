import 'semantic-ui-css/semantic.min.css'
import Dashboard from './layouts/Dashboard';
import React, { useState, useEffect } from 'react';

import { loginRequest } from './utils/authConfig';
import { callMsGraph } from './utils/graph';
import { Button } from 'semantic-ui-react';

import { AuthenticatedTemplate, UnauthenticatedTemplate, useMsal } from '@azure/msal-react';

import TaskList from './pages/TaskLists/TaskList';
import LoginPage from './pages/Login';
import TaskManagement from './pages/TaskManagements/TaskManagement';

const ProfileContent = () => {
  const { instance, accounts } = useMsal();
  const [graphData, setGraphData] = useState(null);

  function RequestProfileData() {
    // Silently acquires an access token which is then attached to a request for MS Graph data
    instance
      .acquireTokenSilent({
        ...loginRequest,
        account: accounts[0],
      })
      .then((response) => {
        callMsGraph(response.accessToken).then((response) => setGraphData(response));
      });
  }

  useEffect(() => {
    RequestProfileData()
  }, [])

  return (
    <>
      <h5 className="card-title">Hoşgeldin {accounts[0].name}</h5>

      {graphData ? (
        <TaskManagement graphData={graphData} />
      ) : (
        <TaskManagement graphData={graphData} />
      )}
    </>
  );
};

const MainContent = () => {
  return (
    <div className="App">
      <AuthenticatedTemplate>
        {/* <TaskList /> */}
        <ProfileContent />
      </AuthenticatedTemplate>

      <UnauthenticatedTemplate>
        <LoginPage />
      </UnauthenticatedTemplate>
    </div>
  );
};



function App() {
  return (
    <div>
      <MainContent />
    </div>
  );
}

export default App;
