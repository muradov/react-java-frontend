/*
 * Copyright (c) Microsoft Corporation. All rights reserved.
 * Licensed under the MIT License.
 */
import { LogLevel } from "@azure/msal-browser";
/**
 * Configuration object to be passed to MSAL instance on creation.
 * For a full list of MSAL.js configuration parameters, visit:
 * https://github.com/AzureAD/microsoft-authentication-library-for-js/blob/dev/lib/msal-browser/docs/configuration.md
 */

export const msalConfig = {
  auth: {
    clientId: "3ff9cbe6-8c93-45cb-a3e8-a197fef37a0d",
    authority: "https://login.microsoftonline.com/fa410ebc-d231-41d6-99e3-6905af821ffb",
  },
  cache: {
    cacheLocation: 'sessionStorage',
    storeAuthStateInCookie: false // Set this to "true" if you are having issues on IE11 or Edge
  },
  system: {
    loggerOptions: {
      loggerCallback: (level, message, containsPii) => {
        if (containsPii) {
          return;
        }
        switch (level) {
          case LogLevel.Error:
            console.error(message);
            return;
          case LogLevel.Info:
            console.info(message);
            return;
          case LogLevel.Verbose:
            console.debug(message);
            return;
          case LogLevel.Warning:
            console.warn(message);
            return;
          default:
            return;
        }
      }
    }
  }
};

/**
 * Scopes you add here will be prompted for user consent during sign-in.
 * By default, MSAL.js will add OIDC scopes (openid, profile, email) to any login request.
 * For more information about OIDC scopes, visit:
 * https://docs.microsoft.com/en-us/azure/active-directory/develop/v2-permissions-and-consent#openid-connect-scopes
 */
export const loginRequest = {
  scopes: [
    "User.Read",
    "Tasks.Read",
    "Tasks.ReadWrite",
    "Calendars.Read",
    "Calendars.ReadBasic",
    "Calendars.ReadWrite",
    "Directory.Read.All",
    "Directory.ReadWrite.All",
    "User.Read",
    "User.Read.All",
    "User.ReadBasic.All",
    "User.ReadWrite",
    "User.ReadWrite.All"
  ],
};

/**
 * Add here the scopes to request when obtaining an access token for MS Graph API. For more information, see:
 * https://github.com/AzureAD/microsoft-authentication-library-for-js/blob/dev/lib/msal-browser/docs/resources-and-scopes.md
 */
export const graphConfig = {
  graphMeEndpoint: "https://graph.microsoft.com/v1.0/me",
};

export const graphConfigGetSchedule = {
  graphMeSchedule: "https://graph.microsoft.com/v1.0/me/calendar/getSchedule",
};

export const calendarAPI = {
  getMyCalendar: "https://graph.microsoft.com/v1.0/me/calendarview?startdatetime=2023-05-29T08:26:51.728Z&enddatetime=2023-06-05T08:26:51.728Z"
};
