import { graphConfig, graphConfigGetSchedule } from "./authConfig";

/**
 * Attaches a given access token to a MS Graph API call. Returns information about the user
 * @param accessToken 
 */
export async function callMsGraph(accessToken) {
    const headers = new Headers();

    const bearer = `Bearer ${accessToken}`;

    headers.append("Authorization", bearer);

    const options = {
        method: "GET",
        headers: headers
    };

    return fetch(graphConfig.graphMeEndpoint, options)
        .then(response => response.json())
        .catch(error => console.log(error));
}


export async function callMsGraphGetSchedule(accessToken, startTime, endTime) {
    const headers = new Headers();

    const bearer = `Bearer ${accessToken}`;

    console.log("startTime: ", startTime);
    console.log("endTime: ", endTime);

    const selectedStartTime = new Date(startTime);
    const selectedEndTime = new Date(endTime);

    const timeOptions = {
        timeZone: 'Europe/Istanbul',
        year: 'numeric',
        month: '2-digit',
        day: '2-digit',
        hour: '2-digit',
        minute: '2-digit',
        second: '2-digit'
    };

    const formattedStartTime = selectedStartTime.toLocaleString('en-GB', timeOptions).replace(/(\d+)\/(\d+)\/(\d+),/, '$3-$2-$1T');
    const formattedEndTime = selectedEndTime.toLocaleString('en-GB', timeOptions).replace(/(\d+)\/(\d+)\/(\d+),/, '$3-$2-$1T');

    const replaceFormattedStartTime = formattedStartTime.replace(" ", "");
    const replaceFormattedEndTime = formattedEndTime.replace(" ", "");

    // console.log("replaceFormattedEndTime: ", replaceFormattedEndTime); // Çıktı: 2023-05-25T11:00:00

    // schedules kısmına mailler eklenecek

    headers.append("Authorization", bearer);
    headers.append("Content-Type", "application/json");

    const jsonData = {
        "schedules": [
            "kaan@avebilisim.com.tr"
        ],
        "startTime": {
            "dateTime": replaceFormattedStartTime,
            "timeZone": "Europe/Istanbul"
        },
        "endTime": {
            "dateTime": replaceFormattedEndTime,
            "timeZone": "Europe/Istanbul"
        },
        "availabilityViewInterval": 60
    };

    console.log("jsonData:", jsonData);

    const options = {
        method: "POST",
        headers: headers,
        body: JSON.stringify(jsonData)
    };

    return fetch(graphConfigGetSchedule.graphMeSchedule, options)
        .then(response => response.json())
        .catch(error => console.log(error));
}
