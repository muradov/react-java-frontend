import React from "react";
import {
  Breadcrumb,
  Divider,
  Container,
  Pagination,
  Table
} from "semantic-ui-react";
import SideBar from "../../layouts/SideBar";
import _ from "lodash";
import "./PrePostTasks.css";

const tableData = [
  { name: "", age: "", gender: "" },
  { name: "", age: "", gender: "Female" },
  { name: "", age: "", gender: "Other" },
  { name: "", age: "", gender: "Male" },
  { name: "", age: "", gender: "" },
  { name: "", age: "", gender: "Female" },
  { name: "", age: "", gender: "Other" },
  { name: "", age: "", gender: "Male" },
  { name: "", age: "", gender: "" },
  { name: "", age: "", gender: "Female" },
  { name: "", age: "", gender: "Other" },
  { name: "", age: "", gender: "Male" },
  { name: "", age: "", gender: "" },
  { name: "", age: "", gender: "Female" },
  { name: "", age: "", gender: "Other" },
  { name: "", age: "", gender: "Male" },
  { name: "", age: "", gender: "Male" },
  { name: "", age: "", gender: "" },
  { name: "", age: "", gender: "Female" },
  { name: "", age: "", gender: "Other" },
  { name: "", age: "", gender: "Male" },
  { name: "", age: "", gender: "Other" },
  { name: "", age: "", gender: "Male" },
];

export default function TaskList() {
  const sections = [
    { key: "Proje Adı", content: "Proje Adı", link: true },
    { key: "Faz", content: "Faz", link: true },
    { key: "İş kırılım kodu", content: "İş kırılım kodu", link: true },
    { key: "İş paketi", content: "İş paketi", link: true },
    { key: "GörevID_Görev", content: "GörevID_Görev", link: true },
    { key: "Öncül ve Ardıl Görevler", content: "Öncül ve Ardıl Görevler", active: true },
  ];

  return (
    <div>
      <SideBar />
      <Breadcrumb icon="right angle" sections={sections} />
      <Divider />
      <div>
        <Container className="" >
          <section>
            <SortableTable />
          </section>
          <footer style={{ display: 'flex', justifyContent: 'center' }}>
            <Pagination defaultActivePage={5} totalPages={10} />
          </footer>
        </Container>
      </div>
    </div>
  );
}

function exampleReducer(state, action) {
  switch (action.type) {
    case "CHANGE_SORT":
      if (state.column === action.column) {
        return {
          ...state,
          data: state.data.slice().reverse(),
          direction:
            state.direction === "ascending" ? "descending" : "ascending",
        };
      }

      return {
        column: action.column,
        data: _.sortBy(state.data, [action.column]),
        direction: "ascending",
      };
    default:
      throw new Error();
  }
}

function SortableTable() {
  const [state, dispatch] = React.useReducer(exampleReducer, {
    column: null,
    data: tableData,
    direction: null,
  });

  const { column, data, direction } = state;

  return (
    <Table sortable celled fixed>
      <Table.Header>
        <Table.Row>
          <Table.HeaderCell
            sorted={column === "name" ? direction : null}
            onClick={() => dispatch({ type: "CHANGE_SORT", column: "name" })}
            style={{ textAlign: 'center' }}
          >
            Öncül veya Ardıl Görevler
          </Table.HeaderCell>
          <Table.HeaderCell
            sorted={column === "age" ? direction : null}
            onClick={() => dispatch({ type: "CHANGE_SORT", column: "age" })}
            style={{ textAlign: 'center' }}
          >
            Alt Görevler
          </Table.HeaderCell>

        </Table.Row>
      </Table.Header>
      <Table.Body>
        {data.map(({ age, gender, name }) => (
          <Table.Row key={name}>
            <Table.Cell>{name}</Table.Cell>
            <Table.Cell>{age}</Table.Cell>

          </Table.Row>
        ))}
      </Table.Body>
    </Table>
  );
}
