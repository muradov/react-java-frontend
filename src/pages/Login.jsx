import React, { useState } from "react";
import { Button, Form, Grid, Header, Segment } from "semantic-ui-react";

import { useNavigate } from "react-router-dom";
import loginService from "../services/loginService/loginService";
import { Providers } from '@microsoft/mgt-element';
import { Msal2Provider } from '@microsoft/mgt-msal2-provider';
import { SignInButton } from "./SignInButton";

import { Login as MgtLogin } from '@microsoft/mgt-react';

// Providers.globalProvider = new Msal2Provider({
//     clientId: '79de5e14-0388-4a58-bcc5-30bf4aadad34'
// });

export default function LoginPage() {
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [failLogin, setFailLogin] = useState("");
    const navigate = useNavigate();

    const handleLoginCompleted = () => {
        navigate("/tasklist"); // Navigate to '/tasklist' route
    };

    const handleSubmit = (e) => {
        e.preventDefault();

        const data = {
            user_name: email,
            password: password,
        };
        loginService()
            .postLogin(data)
            .then((response) => {
                console.log("response: ", response);
                const responseArray = Object.entries(response);
                console.log("responseArray: ", responseArray);
                console.log("responseArray00: ", responseArray[0][0]);
                console.log("responseArray01: ", responseArray[0][1]);
                var id = responseArray[0][1];
                console.log("login page user_id: ", id);
                sessionStorage.setItem("user_id", JSON.stringify(id));
                navigate("/tasklist"); // Navigate to '/tasklist' route
            })
            .catch((error) => {
                console.log("error: ", error);
                setFailLogin("Mail veya şifre hatalı!!!");
            });
    };

    return (
        <div>
            <Grid textAlign="center" style={{ height: "80vh" }} verticalAlign="middle">
                <Grid.Column style={{ maxWidth: 550 }}>
                    <img
                        src={require("../images/proopslogo.png")}
                        style={{ height: "130px", width: "300px" }}
                    />
                    <Header as="h2" color="teal" textAlign="center"></Header>
                    <Form size="large" onSubmit={handleSubmit}>
                        <Segment stacked>
                            <Form.Input
                                fluid
                                icon="user"
                                iconPosition="left"
                                placeholder="Email"
                                value={email}
                                onChange={(e) => {
                                    setEmail(e.target.value);
                                }}
                            />
                            <Form.Input
                                fluid
                                icon="lock"
                                iconPosition="left"
                                placeholder="Password"
                                type="password"
                                value={password}
                                onChange={(e) => {
                                    setPassword(e.target.value);
                                }}
                            />
                            <Button color="teal" fluid size="large" type="submit">
                                Login
                            </Button>
                        </Segment>
                        <header>
                            {
                                // <MgtLogin loginCompleted={handleLoginCompleted} />
                                <SignInButton/>
                            }
                        </header>
                    </Form>
                    {failLogin}
                </Grid.Column>
            </Grid>
        </div>
    );
}

