import React, { useState, useEffect, useRef } from "react";
import {
    Header,
    Breadcrumb,
    Divider,
    Form,
    Comment,
    Segment,
    Container,
    Checkbox,
    Button
} from "semantic-ui-react";
import SideBar from "../../layouts/SideBar";
// import "./TaskComments.css";
import attachmentService from "../../services/attachmentService/attachmentService";
import commentService from "../../services/commentService/commentService";
import axios from 'axios';

export default function TaskComments() {

    const REACT_APP_ADD_COMMENT = process.env.REACT_APP_ADD_COMMENT

    const sections = [
        { key: "Proje Adı", content: "Proje Adı", link: true },
        { key: "Faz", content: "Faz", link: true },
        { key: "İş kırılım kodu", content: "İş kırılım kodu", link: true },
        { key: "İş paketi", content: "İş paketi", link: true },
        { key: "GörevID_Görev", content: "GörevID_Görev", link: true },
        { key: "Görev Yorumları", content: "Görev Yorumları", active: true },
    ];


    // const [comment, setComment] = useState("");


    const [start_date, setStartDate] = useState("");
    const [end_date, setEndDate] = useState("");
    const [time_difference, setTimeDifference] = useState("");
    const [hours_difference, setHoursDifference] = useState("");
    const [minutes_difference, setMinutesDifference] = useState("");
    const [show_calculation, setShowCalculation] = useState(false);

    function getTimeDifference() {
        const start_date_object = new Date(start_date);
        const end_date_object = new Date(end_date);
        const time_difference = end_date_object.getTime() - start_date_object.getTime();
        setTimeDifference(time_difference);

        const hours_difference = Math.floor(time_difference / (1000 * 3600));
        setHoursDifference(hours_difference);

        const minutes_difference = Math.floor((time_difference / (1000 * 60)) % 60);
        setMinutesDifference(minutes_difference);
    }

    const [text, setYorumDetay] = useState("");
    const personId = 15;
    let efor = hours_difference;
    const taskId = 1;
    const [file, setFile] = useState(null);
    const [refresh, setRefresh] = useState([false]);
    const [fileInputKey, setFileInputKey] = useState(0);

    const handleDownload = (fileId) => {
        window.open(fileId);
    }

    const handleFileChange = (event) => {
        console.log(event.target.value);
        setFile(event.target.files[0]); // dosya nesnesi seçildiğinde file state'i güncellenir

        const formData = new FormData();
        formData.append("file", event.target.files[0]);

        attachmentService()
            .postFile(formData)
            .then((response) => {
                console.log(response);
            })
            .catch((error) => {
                console.error(error);
            });
    };

    const handleSubmit = (event) => {
        var taskId = sessionStorage.getItem("task_id");
        var personId = sessionStorage.getItem("user_id");
        var role_id = sessionStorage.getItem("role_id");
        // 4 3 5 2
        if (role_id === 1 || role_id === 2) {
            console.log("1");
        }
        event.preventDefault();
        // check if text is not empty
        setYorumDetay("");
        setHoursDifference("");
        setStartDate("");
        setEndDate("");
        setShowCalculation(false);
        setFileInputKey(fileInputKey + 1);


        if (text.trim() !== "") {
            // Create an object with the parameter values
            const data = new FormData();
            data.append('text', text);
            if (file !== null) {
                data.append('file', file);
            }
            data.append('personId', personId);
            data.append('efor', efor);
            data.append('taskId', taskId);

            // Make a POST request to the server using Axios
            axios.post(REACT_APP_ADD_COMMENT, data)

                .then(response => {
                    setRefresh(true);
                    console.log(response.data);
                })
                .catch(error => {
                    console.error(error);
                });
        }
    }

    const handleYorumDetayChange = (event) => {
        setYorumDetay(event.target.value);
    }

    const [commentt] = useState("");
    const [comments, setComments] = useState([]);
    useEffect(() => {
        var taskId = sessionStorage.getItem("task_id");
        // Fetch comments from server using the getComments function
        commentService()
            .getComments(taskId)
            .then((data) => {
                // Set the comments state to the array of comments returned by the server
                setComments(data);
            })
            .catch((error) => {
                console.log(error);
            });
        if (refresh) {
            setRefresh(false); // reset refresh to false after comments state is updated
        }
    }, [refresh]);



    return (
        <div>
            <SideBar />
            <Container>
                <Breadcrumb icon="right angle" sections={sections} />
                <Divider />
                <Form.Group>
                    <Form onSubmit={handleSubmit}>
                        <div style={{ display: 'flex' }}>
                            <Form.Input type="file" icon="attach" onChange={handleFileChange} key={fileInputKey} />
                            <Form.TextArea
                                width="16"
                                height="100px"
                                input="text"
                                id="text"
                                name="text"
                                placeholder="Yorum ekleyiniz..."
                                value={text}
                                onChange={handleYorumDetayChange}
                            />
                        </div>
                        <Form.Button
                            icon="plus circle"
                            labelPosition="right"
                            content="Yorum Ekle"
                            floated="right"
                            type="submit"
                        />

                    </Form>
                </Form.Group>
                <Checkbox label="Efor ?" checked={show_calculation} onChange={() => setShowCalculation(!show_calculation)} />

                {show_calculation && (
                    <div>
                        <label htmlFor="start_date">Start Date:</label>
                        <input type="datetime-local" id="start_date" value={start_date} onChange={(e) => setStartDate(e.target.value)} />

                        <label htmlFor="end_date">End Date:</label>
                        <input type="datetime-local" id="end_date" value={end_date} onChange={(e) => setEndDate(e.target.value)} />

                        <button onClick={getTimeDifference}>Efor Hesapla</button>

                        <label htmlFor="hours">Saat:</label>
                        <input type="text" id="hours" value={hours_difference} readOnly />

                    </div>
                )}
                <Segment basic textAlign="center" disabled>
                    <Divider />
                </Segment>

                <Comment.Group className="comment-group">
                    {comments.map((comment, index) => (
                        <Comment key={index}>
                            <Comment.Avatar
                                as='a'
                                src="https://react.semantic-ui.com/images/avatar/small/stevie.jpg"
                            />
                            <div>
                                <Comment.Content>
                                    <div className="comment-header">
                                        <div>
                                            <Comment.Author>{comment.username}</Comment.Author>
                                        </div>
                                        <div>
                                            <Comment.Metadata>
                                                {comment.gerceklesenBaslangicTarihi}
                                            </Comment.Metadata>
                                        </div>
                                    </div>
                                    <div className="comment-body">
                                        <div>
                                            <Comment.Text>
                                                {comment.yorumDetay}
                                            </Comment.Text>
                                        </div>
                                        <div className="comment-footer">
                                            <div>
                                                Gerçekleşen Efor: {comment.gerceklesenEfor}
                                            </div>
                                            <div>
                                                {comment.fileId && (<a href={comment.fileId} download>Dosyayı İndir</a>)}
                                            </div>
                                        </div>
                                    </div>
                                </Comment.Content>
                            </div>
                        </Comment>
                    ))}
                </Comment.Group>

                <style>
                    {`
                    .comment-group{
                    max-width:none !important;
                    }
                      .comment-header {
                        display: flex;
                        justify-content: space-between;
                      }

                      .comment-body {
                        
                        margin-top: 3px;
                      }

                      .comment-footer {
                        display: flex;
                        justify-content: space-between;
                      }
        `}
                </style>


            </Container>
        </div>
    );
}
