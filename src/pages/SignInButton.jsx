import React from "react";
import { useMsal } from "@azure/msal-react";
import { loginRequest } from "../utils/authConfig";
import { Button } from 'semantic-ui-react';

export const SignInButton = () => {
  const { instance } = useMsal();

  const handleLogin = (loginType) => {
   if (loginType === "redirect") {
      instance.loginRedirect(loginRequest).catch((e) => {
        console.log(e);
      });
    }
  };
  return (
    <Button
      variant="secondary"
      className="ml-auto"
      drop="start"
      title="Sign In"
    >
      <Button as="button" onClick={() => handleLogin("redirect")}>
        Sign in using Redirect
      </Button>
    </Button>
  );
};