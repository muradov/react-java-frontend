import React, { useState, useEffect } from "react";
import { Calendar } from "primereact/calendar";
import "primereact/resources/themes/md-light-indigo/theme.css";
import "primereact/resources/primereact.min.css";
import "primeicons/primeicons.css";
import {
  Breadcrumb,
  Button,
  Divider,
  Dropdown,
  Form,
  Message,
  TextArea,
} from "semantic-ui-react";
import SideBar from "../../layouts/SideBar";
import projectService from "../../services/projectService/projectService";
import taskManagement from "../../services/taskService/taskManagement";
import taskDataServices from "../../services/taskService/taskType.js/taskTypeData";
import priorityService from "../../services/taskService/taskPriorityDataService";
import taskSituationDataService from "../../services/taskService/taskSituationDataService";
import personService from "../../services/personService/personService";
import taskProgressDataService from "../../services/taskService/taskProgressData";
import { AuthenticatedTemplate, UnauthenticatedTemplate, useMsal } from '@azure/msal-react';
import { loginRequest } from '../../utils/authConfig';
import { callMsGraphGetSchedule } from '../../utils/graph';

export default function TaskManagement(props) {

  console.log("props TaskManagement: ", props);

  const projectManager = [
    { key: "1", text: "Harun Çetin", value: 1 },
    { key: "2", text: "Tuba Öngören", value: 2 },
    { key: "3", text: "Hülya Keleş", value: 3 },
    { key: "4", text: "Bircan Ataş", value: 4 },
  ];

  const [personList, setPersonList] = useState()
  const [pyPersonList, setPyPersonList] = useState()

  const [taskTypeData, setTaskTypeData] = useState()

  const [priorityData, setPriorityData] = useState()

  const [situationData, setSituationData] = useState()

  const [progressData, setProgressData] = useState()

  const sections = [
    { key: "Proje Adı", content: "Proje Adı", link: true },
    { key: "Faz", content: "Faz", link: true },
    { key: "İş kırılım kodu", content: "İş kırılım kodu", link: true },
    { key: "İş paketi", content: "İş paketi", link: true },
    { key: "GörevID_Görev", content: "GörevID_Görev", active: true },
  ];


  const [taskName, setTaskName] = useState("");
  const [projectManagerId, setProjectManagerId] = useState("");
  const [responsibleId, setResponsibleId] = useState("");
  const [advisorId, setAdvisorId] = useState("");
  const [productOwner, setProductOwner] = useState("");
  const [technicalLeaderId, setTechnicalLeaderId] = useState("");
  const [plannedStartDate, setPlannedStartDate] = useState(new Date());
  const [plannedEndDate, setPlannedEndDate] = useState(new Date());

  const [actualStartDate, setActualStartDate] = useState(new Date());
  const [actualEndDate, setActualEndDate] = useState(new Date());

  const [plannedEffort, setPlannedEffort] = useState("");
  const [effortMade, setEffortMade] = useState("");
  const [taskType, setTaskType] = useState("");
  const [priority, setPriority] = useState("");
  const [situation, setSituation] = useState("");
  const [progress, setProgress] = useState("");
  const [taskDescription, setTaskDescription] = useState("");

  const [message, setMessage] = useState(null);

  const [projectId, setProjectId] = useState(0);

  const [projects, setProjects] = useState();

  // BÜTÜN ALANLAR BURADAN GELİR
  useEffect(() => {
    // tüm projeleri getirir
    projectService()
      .getAllProject()
      .then((response) => {
        var transformedProjects = response.map((result) => ({
          key: result.id,
          text: result.project_name,
          value: result.id,
        }));
        setProjects(transformedProjects);
      })
      .catch((error) => {
        console.error(error);
      });

    // tüm task türlerini getirir
    taskDataServices()
      .getAllTaskType()
      .then((response) => {
        console.log("Task type data:", response);
        var data = response.map((result) => ({
          id: result.id,
          text: result.task_type_data_name,
          value: result.task_type_data_name
        }));
        setTaskTypeData(data)
      }).catch((error) => {
        console.log("Task type data error:", error);
      })

    // tüm öncelik türlerini getirir
    priorityService()
      .getAllPriority()
      .then((response) => {
        console.log("priority_data_name:", response);
        var data = response.map((result) => ({
          id: result.id,
          text: result.priority_data_name,
          value: result.priority_data_name
        }));
        setPriorityData(data)
      }).catch((error) => {
        console.log("priority_data_name:", error);
      })

    // tüm durum türlerini getirir
    taskSituationDataService()
      .getAllSituation()
      .then((response) => {
        console.log("situation_data_name:", response);
        var data = response.map((result) => ({
          id: result.id,
          text: result.situation_data_name,
          value: result.situation_data_name
        }));
        setSituationData(data)
      }).catch((error) => {
        console.log("situation_data_name:", error);
      })

    // tüm progress değerlerini getirir
    taskProgressDataService()
      .getAllProgress()
      .then((response) => {
        console.log("progress_data_name:", response);
        let data = response.map((result) => ({
          id: result.id,
          text: result.progress_data_name,
          value: result.progress_data_name
        }));
        setProgressData(data)
      }).catch((error) => {
        console.log("progress_data_name:", error);
      })


    // tüm personları getirir
    personService()
      .getAllPerson()
      .then((response) => {
        let data = response.sort((a, b) => a.name > b.name ? 1 : -1)
          .map((result) => ({
            id: result.id,
            text: result.name + " " + result.surname,
            value: result.id
          }));
        setPersonList(data)
        console.log(personList);
      }).catch((error) => {
        console.log("person_list:", error);
      })

    // tüm py rolündeki personları getirir
    personService()
      .getAllPyPerson()
      .then((response) => {
        let data = response.sort((a, b) => a.name > b.name ? 1 : -1)
          .map((result) => ({
            id: result.id,
            text: result.name + " " + result.surname,
            value: result.id
          }));
        setPyPersonList(data)
        console.log(personList);
      }).catch((error) => {
        console.log("person_list:", error);
      })

  }, []);

  const { instance, accounts } = useMsal();
  const [graphData, setGraphData] = useState(null);

  // Proje ekler
  const handleSubmit = (e) => {
    e.preventDefault();

    // Silently acquires an access token which is then attached to a request for MS Graph data
    instance
      .acquireTokenSilent({
        ...loginRequest,
        account: accounts[0],
      })
      .then((response) => {
        callMsGraphGetSchedule(response.accessToken, plannedStartDate, plannedEndDate).then((response) => setGraphData(response));
      });


    let result = graphData.value;

    // console.log("graphData: ", result[0].scheduleItems.length); // eğer takvimde veri yoksa hata veriyor

    if (result[0].scheduleItems.length !== 0) {
      console.log("Takvim aralıklarında kişinin görevi mevcut");
    }
    else {
      console.log("takvim aralıkları boş");
      const formattedDatePlannedStartDate =
        plannedStartDate.toLocaleDateString("tr-TR");
      console.log("formattedDate: ", formattedDatePlannedStartDate); // 18.04.2023

      const formattedDatePlannedEndDate =
        plannedEndDate.toLocaleDateString("tr-TR");
      console.log("formattedDate: ", formattedDatePlannedEndDate); // 18.04.2023

      const formattedDateActualStartDate =
        actualStartDate.toLocaleDateString("tr-TR");
      console.log("formattedDate: ", formattedDateActualStartDate); // 18.04.2023

      const formattedDateActualEndDate =
        actualEndDate.toLocaleDateString("tr-TR");
      console.log("formattedDate: ", formattedDateActualEndDate); // 18.04.2023

      const data = {
        project_id: projectId,
        task_name: taskName,
        product_owner_id: productOwner,
        responsible_id: responsibleId,
        project_manager_id: projectManagerId,
        advisor_id: advisorId,
        technical_leader_id: technicalLeaderId,
        planned_start_date: formattedDatePlannedStartDate,
        planned_end_date: formattedDatePlannedEndDate,
        planned_effort: plannedEffort,
        task_type: taskType,
        priority: priority,
        situation: situation,
        progress: progress,
        actual_start_date: formattedDateActualStartDate,
        actual_end_date: formattedDateActualEndDate,
        task_description: taskDescription,
      };


      taskManagement()
        .addTask(data)
        .then((response) => {
          console.log("response:", response);
          showMessage(response);
        })
        .catch((error) => {
          console.error("error: ", error);
        });
    }


  };


  const resetForm = () => {
    setTaskName("")
    setProjectManagerId("")
    setResponsibleId("")
    setAdvisorId("")
    setProductOwner("")
    setTechnicalLeaderId("")
    setPlannedStartDate("")
    setSituation("")
    setPlannedEndDate("")
    setActualStartDate("")
    setActualEndDate("")
    setPlannedEffort("")
    setEffortMade("")
    setTaskType("")
    setPriority("")
    setTaskDescription("")
    setMessage("")
    setProjectId("")
    setProjects("")
    setProgress("")
  }

  const showMessage = (message) => {
    setMessage(message); // task eklendikten sonra gelen bildirim mesajı
    resetForm()
    setTimeout(() => {
      setMessage(null);
    }, 3000); // 3 saniye beklet

  };

  const [disable, setDisable] = useState(false);

  return (
    <div>
      <SideBar />
      <Breadcrumb icon="right angle" sections={sections} />
      <Divider />
      <Dropdown
        placeholder="Projeler"
        fluid
        selection
        options={projects}
        value={projectId}
        disabled={disable}
        onChange={(e, data) => setProjectId(data.value)}
        style={{ width: "300px", marginRight: "50px" }}
      />
      <br />
      <Form>
        <Form.Input
          placeholder="Başlık"
          style={{ width: "1340px" }}
          value={taskName}
          disabled={disable}
          onChange={(e) => setTaskName(e.target.value)}
        />
      </Form>
      <div style={{ display: "flex", marginTop: "20px" }}>
        <Dropdown
          placeholder={"Proje Yöneticisi"}
          fluid
          selection
          clearable
          options={pyPersonList}
          value={projectManagerId}
          disabled={disable}
          onChange={(e, data) => setProjectManagerId(data.value)}
          style={{ width: "300px", marginRight: "50px" }}
        />

        <Dropdown
          placeholder="Görev Sorumlusu"
          fluid
          selection
          clearable
          options={personList}
          value={responsibleId}
          disabled={disable}
          onChange={(e, data) => setResponsibleId(data.value)}
          style={{ width: "300px", marginRight: "50px" }}
        />

        <Dropdown
          placeholder="Teknik Lider"
          fluid
          selection
          clearable
          options={personList}
          disabled={disable}
          value={technicalLeaderId}
          onChange={(e, data) => setTechnicalLeaderId(data.value)}
          style={{ width: "300px", marginRight: "50px" }}
        />

        <Dropdown
          placeholder="Danışman"
          fluid
          selection
          clearable
          options={personList}
          disabled={disable}
          value={advisorId}
          onChange={(e, data) => setAdvisorId(data.value)}
          style={{ width: "300px", marginRight: "50px" }}
        />

        <Dropdown
          placeholder="Ürün Sorumlusu"
          fluid
          selection
          clearable
          options={personList}
          value={productOwner}
          disabled={disable}
          onChange={(e, data) => setProductOwner(data.value)}
          style={{ width: "300px", marginRight: "50px" }}
        />
      </div>
      <div style={{ display: "flex", justifyContent: "space-between" }}>
        <Dropdown
          placeholder="Görev Türü"
          fluid
          selection
          options={taskTypeData}
          clearable
          wrapSelection={false}
          disabled={disable}
          value={taskType}
          onChange={(e, data) => setTaskType(data.value)}
          style={{ width: "300px", marginTop: "40px" }}
        />
        <Dropdown
          placeholder="Öncelik"
          fluid
          selection
          options={priorityData}
          clearable
          wrapSelection={false}
          disabled={disable}
          value={priority}
          onChange={(e, data) => setPriority(data.value)}
          style={{ width: "300px", marginTop: "40px" }}
        />
        <Dropdown
          placeholder="Durum"
          fluid
          selection
          options={situationData}
          clearable
          wrapSelection={false}
          value={situation}
          disabled={disable}
          onChange={(e, data) => setSituation(data.value)}
          style={{ width: "300px", marginTop: "40px" }}
        />

        <Dropdown
          placeholder="İlerleme Durumu"
          fluid
          selection
          options={progressData}
          clearable
          disabled={disable}
          wrapSelection={false}
          value={progress}
          onChange={(e, data) => setProgress(data.value)}
          style={{ width: "300px", marginTop: "40px" }}
        />
      </div>
      <div style={{ display: "flex", justifyContent: "space-between" }}>
        <Calendar
          showTime
          hourFormat="24"
          value={plannedStartDate}
          onChange={(e) => setPlannedStartDate(e.target.value)}
          showIcon
          style={{ width: "400px", marginTop: "30px", height: "40px" }}
          placeholder={"Planlanan Başlangıç Tarihi"}
        />
        <Calendar
          showTime
          hourFormat="24"
          value={plannedEndDate}
          onChange={(e) => setPlannedEndDate(e.value)}
          showIcon
          disabled={disable}
          style={{ width: "400px", marginTop: "30px", height: "40px" }}
          placeholder={"Planlanan Bitiş Tarihi"}
        />
        <div style={{ width: "400px", marginTop: "30px", height: "40px" }}>
          <Form>
            <Form.Input
              placeholder="Planlanan Efor"
              value={plannedEffort}
              type="number"
              min="0"
              clearable
              disabled={disable}
              onChange={(e) => setPlannedEffort(e.target.value)}
            />
          </Form>
        </div>
      </div>
      <div style={{ display: "flex", justifyContent: "space-between" }}>
        <Calendar
          value={actualStartDate}
          onChange={(e) => setActualStartDate(e.value)}
          showIcon
          disabled={disable}
          style={{ width: "400px", marginTop: "30px", height: "40px" }}
          placeholder={"Gerçeklenen Başlangıç Tarihi"}
        />
        <Calendar
          value={actualEndDate}
          onChange={(e) => setActualEndDate(e.value)}
          showIcon
          disabled={disable}
          style={{ width: "400px", marginTop: "30px", height: "40px" }}
          placeholder={"Gerçeklenen Bitiş Tarihi"}
        />
        <div style={{ width: "400px", marginTop: "30px" }}>
          <Form>
            <Form.Input
              placeholder="Gerçekleşen Efor"
              type="number"
              min="0"
              disabled={disable}
              value={effortMade}
              onChange={(e) => setEffortMade(e.target.value)}
            />
          </Form>
        </div>
      </div>
      {message && (
        <Message>
          <p>{message}</p>
        </Message>
      )}
      <div style={{ marginTop: "190px" }}>
        <Form>
          <TextArea
            placeholder="Buraya görev tanımı ekleyin"
            value={taskDescription}
            disabled={disable}
            onChange={(e) => setTaskDescription(e.target.value)}
          />
        </Form>
      </div>
      <br />

      <Button primary onClick={handleSubmit}>
        Görev Ekle
      </Button>

    </div>
  );
}
