import React, { useState, useEffect } from "react";
import { Calendar } from "primereact/calendar";
import "primereact/resources/themes/md-light-indigo/theme.css";
import "primereact/resources/primereact.min.css";
import "primeicons/primeicons.css";
import {
  Breadcrumb,
  Button,
  Divider,
  Dropdown,
  Form,
  Message,
  TextArea,
} from "semantic-ui-react";
import SideBar from "../../layouts/SideBar";
import { Link } from "react-router-dom";
import taskManagement from "../../services/taskService/taskManagement";
import taskProgressDataService from "../../services/taskService/taskProgressData";
import taskDataServices from "../../services/taskService/taskType.js/taskTypeData";
import priorityService from "../../services/taskService/taskPriorityDataService";
import taskSituationDataService from "../../services/taskService/taskSituationDataService";

export default function TaskManagement() {

  const projectManager = [
    { key: "1", text: "Harun Çetin", value: 1 },
    { key: "2", text: "Tuba Öngören", value: 2 },
    { key: "3", text: "Hülya Keleş", value: 3 },
    { key: "4", text: "Bircan Ataş", value: 4 },
  ];
  

  const [taskTypeData, setTaskTypeData] = useState()

  const [priorityData, setPriorityData] = useState()

  const [situationData, setSituationData] = useState()

  const [progressList, setProgressList] = useState()

  const sections = [
    { key: "Proje Adı", content: "Proje Adı", link: true },
    { key: "Faz", content: "Faz", link: true },
    { key: "İş kırılım kodu", content: "İş kırılım kodu", link: true },
    { key: "İş paketi", content: "İş paketi", link: true },
    { key: "GörevID_Görev", content: "GörevID_Görev", active: true },
  ];


  const [taskName, setTaskName] = useState("");
  const [projectManagerId, setProjectManagerId] = useState("");
  const [responsibleId, setResponsibleId] = useState("");
  const [advisorId, setAdvisorId] = useState("");
  const [productOwner, setProductOwner] = useState("");
  const [technicalLeaderId, setTechnicalLeaderId] = useState("");
  const [plannedStartDate, setPlannedStartDate] = useState(new Date());
  const [plannedEndDate, setPlannedEndDate] = useState(new Date());

  const [actualStartDate, setActualStartDate] = useState(new Date());
  const [actualEndDate, setActualEndDate] = useState(new Date());

  const [plannedEffort, setPlannedEffort] = useState("");
  const [effortMade, setEffortMade] = useState(0);
  const [taskType, setTaskType] = useState("");
  const [priority, setPriority] = useState("");
  const [situation, setSituation] = useState("");
  const [taskDescription, setTaskDescription] = useState("");
  const [progress, setProgress] = useState("");

  let task_id = sessionStorage.getItem("task_id");
  let person_id = sessionStorage.getItem("user_id");


  // task bilgisini getirir.
  useEffect(() => {
    taskManagement()
      .getTask(task_id)
      .then((response) => {
        console.log("getTask:", response);
        setTaskName(response[0].task_name);
        setProjectManagerId(response[0].project_manager_name);
        setResponsibleId(response[0].responsible_name);
        setAdvisorId(response[0].advisor_name);
        setProductOwner(response[0].product_owner_name);
        setTechnicalLeaderId(response[0].technical_leader_name);

        let planned_start_date = splitDateTime(response[0].planned_start_date);
        setPlannedStartDate(planned_start_date);

        let planned_end_date = splitDateTime(response[0].planned_end_date);
        setPlannedEndDate(planned_end_date);

        let actual_start_date = splitDateTime(response[0].actual_start_date);
        setActualStartDate(actual_start_date);

        let actual_end_date = splitDateTime(response[0].actual_end_date);
        setActualEndDate(actual_end_date);

        setPlannedEffort(response[0].planned_effort);
        setTaskType(response[0].task_type_name)
        setPriority(response[0].priority_name);
        setSituation(response[0].situation_name);
        setTaskDescription(response[0].task_description);
        setProgress(response[0].progress_name);

      })
      .catch((error) => {
        console.error(error);
      });
  }, []);

  const [inputDisable, setInputDisable] = useState(false);
  const [dropDownProjectManager, setDropDownProjectManager] = useState(false);
  const [dropDownResponsible, setDropDownResponsible] = useState(false);
  const [dropDownTechnicalLeader, setDropDownTechnicalLeader] = useState(false);
  const [dropDownSetAdvisor, setDropDownSetAdvisor] = useState(false);
  const [dropDownProductOwner, setDropDownProductOwner] = useState(false);
  const [dropDownTaskType, setDropDownTaskType] = useState(false);
  const [dropDownPriority, setDropDownPriority] = useState(false);
  const [dropDownSituation, setDropDownSituation] = useState(false);
  const [dropDownProgress, setDropDownProgress] = useState(false) // progress
  const [calendarPlannedStartDate, setCalendarPlannedStartDate] = useState(false);
  const [calendarPlannedEndDate, setCalendarPlannedEndDate] = useState(false);
  const [calendarActualStartDate, setCalendarActualStartDate] = useState(false);
  const [calendarActualEndDate, setCalendarActualEndDate] = useState(false);
  const [textAreaTaskDescription, setTextAreaTaskDescription] = useState(false);
  const [inputPlannedEffort, setInputPlannedEffort] = useState(false);
  const [upDateButton, setUpDateButton] = useState(false);
  const [inputEffortMade, setInputEffortMade] = useState(false);

  // task'a ve person id'ye göre kişiye atanan yetki ayarları
  useEffect(() => {
    taskManagement()
      .getRoleIdByTaskIdAndPersonId(task_id, person_id)
      .then((response) => {
        let role_id = response.taskRoleId;
        console.log("role_id:", role_id);
        console.log("task_id:", task_id);

        sessionStorage.setItem("role_id", JSON.stringify(role_id));

        switch (role_id) {
          case 1:
            console.log("1"); // Proje Yöneticisi
            setCalendarPlannedStartDate(false);
            setTextAreaTaskDescription(false);
            setDropDownProgress(false)
            break;
          case 2:
            console.log("2"); //
            break;
          default: // true iken kapalıdır!!!!
            setInputDisable(true);
            setDropDownProjectManager(true);
            setDropDownResponsible(true);
            setDropDownTechnicalLeader(true);
            setDropDownSetAdvisor(true);
            setDropDownProductOwner(true);
            setDropDownTaskType(true);
            setDropDownPriority(true);
            setDropDownSituation(true);
            setCalendarPlannedStartDate(true);
            setCalendarPlannedEndDate(true);
            setCalendarActualStartDate(true);
            setCalendarActualEndDate(true);
            setTextAreaTaskDescription(true);
            setInputPlannedEffort(true);
            setUpDateButton(true);
            setInputEffortMade(true);
        }
      })
      .catch((error) => {
        console.error(error);
      });
  }, []);

  // task'in efor miktarını getirir.
  useEffect(() => {
    try {
      taskManagement()
        .getTaskByIdEffort(task_id)
        .then((response) => {
          setEffortMade(response);
          return response;
        })
        .catch((error) => {
          setEffortMade(0);
          throw error;
        });
    } catch (error) {
      console.log(error);
    }
  }, []);


  // task progress oranını getirir.
  useEffect(() => {
    taskProgressDataService()
      .getAllProgress()
      .then((response) => {
        console.log("progress_data_name:", response);
        let data = response.map((result) => ({
          id: result.id,
          text: result.progress_data_name,
          value: result.progress_data_name
        }));
        setProgressList(data)
      }).catch((error) => {
        console.log("progress_data_name:", error);
      })

    taskDataServices()
      .getAllTaskType()
      .then((response) => {
        console.log("Task type data:", response);
        var data = response.map((result) => ({
          id: result.id,
          text: result.task_type_data_name,
          value: result.task_type_data_name
        }));
        setTaskTypeData(data)
      }).catch((error) => {
        console.log("Task type data error:", error);
      })

    // tüm öncelik türlerini getirir
    priorityService()
      .getAllPriority()
      .then((response) => {
        console.log("priority_data_name:", response);
        var data = response.map((result) => ({
          id: result.id,
          text: result.priority_data_name,
          value: result.priority_data_name
        }));
        setPriorityData(data)
      }).catch((error) => {
        console.log("priority_data_name:", error);
      })

    // tüm durum türlerini getirir
    taskSituationDataService()
      .getAllSituation()
      .then((response) => {
        console.log("situation_data_name:", response);
        var data = response.map((result) => ({
          id: result.id,
          text: result.situation_data_name,
          value: result.situation_data_name
        }));
        setSituationData(data)
      }).catch((error) => {
        console.log("situation_data_name:", error);
      })

  }, [])




  const handleUpdateTask = (props) => {
    props.preventDefault();
    const formattedDatePlannedStartDate =
      plannedStartDate.toLocaleDateString("tr-TR");
    console.log("formattedDate: ", formattedDatePlannedStartDate); // 18.04.2023

    const formattedDatePlannedEndDate =
      plannedEndDate.toLocaleDateString("tr-TR");
    console.log("formattedDate: ", formattedDatePlannedEndDate); // 18.04.2023

    const formattedDateActualStartDate =
      actualStartDate.toLocaleDateString("tr-TR");
    console.log("formattedDate: ", formattedDateActualStartDate); // 18.04.2023

    const formattedDateActualEndDate =
      actualEndDate.toLocaleDateString("tr-TR");
    console.log("formattedDate: ", formattedDateActualEndDate); // 18.04.2023

    const data = {
      task_name: taskName,
      product_owner_id: productOwner,
      responsible_id: responsibleId,
      project_manager_id: projectManagerId,
      advisor_id: advisorId,
      technical_leader_id: technicalLeaderId,
      planned_start_date: formattedDatePlannedStartDate,
      planned_end_date: formattedDatePlannedEndDate,
      planned_effort: plannedEffort,
      task_type: taskType,
      priority: priority,
      situation: situation,
      actual_start_date: formattedDateActualStartDate,
      actual_end_date: formattedDateActualEndDate,
      task_description: taskDescription,
      progress_name: progress,
    };


    taskManagement()
      .updateTask(props, data)
      .then((response) => {
        console.log("hata");
        return response;
      })
      .catch((error) => {
        setEffortMade(0);
        throw error;
      });
  };

  return (
    <div>
      <SideBar />
      <Breadcrumb icon="right angle" sections={sections} />
      <Divider />
      <Form>
        <Form.Input
          placeholder="Başlık"
          style={{ width: "1340px" }}
          value={taskName}
          disabled={inputDisable}
          onChange={(e) => setTaskName(e.target.value)}
        />
      </Form>
      <div style={{ display: "flex", marginTop: "20px" }}>
        <Dropdown
          placeholder={projectManagerId}
          fluid
          selection
          options={projectManager}
          value={projectManagerId}
          disabled={dropDownProjectManager}
          onChange={(e, data) => setProjectManagerId(data.value)}
          style={{ width: "300px", marginRight: "50px" }}
        />

        <Dropdown
          placeholder={responsibleId}
          fluid
          selection
          options={projectManager}
          disabled={dropDownResponsible}
          value={responsibleId}
          onChange={(e, data) => setResponsibleId(data.value)}
          style={{ width: "300px", marginRight: "50px" }}
        />

        <Dropdown
          placeholder={technicalLeaderId}
          fluid
          selection
          options={projectManager}
          disabled={dropDownTechnicalLeader}
          value={technicalLeaderId}
          onChange={(e, data) => setTechnicalLeaderId(data.value)}
          style={{ width: "300px", marginRight: "50px" }}
        />

        <Dropdown
          placeholder={advisorId}
          fluid
          selection
          options={projectManager}
          disabled={dropDownSetAdvisor}
          value={advisorId}
          onChange={(e, data) => setAdvisorId(data.value)}
          style={{ width: "300px", marginRight: "50px" }}
        />

        <Dropdown
          placeholder={productOwner}
          fluid
          selection
          options={projectManager}
          value={productOwner}
          disabled={dropDownProductOwner}
          onChange={(e, data) => setProductOwner(data.value)}
          style={{ width: "300px", marginRight: "50px" }}
        />
      </div>
      <div style={{ display: "flex", justifyContent: "space-between" }}>
        <Dropdown
          placeholder={taskType}
          fluid
          selection
          options={taskTypeData}
          clearable
          wrapSelection={false}
          disabled={dropDownTaskType}
          value={taskType}
          onChange={(e, data) => setTaskType(data.value)}
          style={{ width: "300px", marginTop: "40px" }}
        />
        <Dropdown
          placeholder="Öncelik"
          fluid
          selection
          options={priorityData}
          clearable
          wrapSelection={false}
          disabled={dropDownPriority}
          value={priority}
          onChange={(e, data) => setPriority(data.value)}
          style={{ width: "300px", marginTop: "40px" }}
        />
        <Dropdown
          placeholder={situation}
          fluid
          selection
          options={situationData}
          clearable
          wrapSelection={false}
          disabled={dropDownSituation}
          value={situation}
          onChange={(e, data) => setSituation(data.value)}
          style={{ width: "300px", marginTop: "40px" }}
        />
        <Dropdown
          placeholder={progress}
          fluid
          selection
          options={progressList}
          clearable
          wrapSelection={false}
          value={progress}
          disabled={dropDownProgress}
          onChange={(e, data) => setProgress(data.value)}
          style={{ width: "300px", marginTop: "40px" }}
        />
      </div>
      <div style={{ display: "flex", justifyContent: "space-between" }}>
        <Calendar
          value={plannedStartDate}
          onChange={(e) => setPlannedStartDate(e.target.value)}
          showIcon
          disabled={calendarPlannedStartDate}
          style={{ width: "400px", marginTop: "30px", height: "40px" }}
          placeholder={"Planlanan Başlangıç Tarihi"}
        />
        <Calendar
          value={plannedEndDate}
          onChange={(e) => setPlannedEndDate(e.value)}
          showIcon
          disabled={calendarPlannedEndDate}
          style={{ width: "400px", marginTop: "30px", height: "40px" }}
          placeholder={"Planlanan Bitiş Tarihi"}
        />
        <div style={{ width: "400px", marginTop: "30px", height: "40px" }}>
          <Form>
            <Form.Input
              placeholder="Planlanan Efor"
              value={plannedEffort}
              type="number"
              min="0"
              disabled={inputPlannedEffort}
              onChange={(e) => setPlannedEffort(e.target.value)}
            />
          </Form>
        </div>
      </div>
      <div style={{ display: "flex", justifyContent: "space-between" }}>
        <Calendar
          value={actualStartDate}
          onChange={(e) => setActualStartDate(e.value)}
          showIcon
          disabled={calendarActualStartDate}
          style={{ width: "400px", marginTop: "30px", height: "40px" }}
          placeholder={"Gerçeklenen Başlangıç Tarihi"}
        />
        <Calendar
          value={actualEndDate}
          onChange={(e) => setActualEndDate(e.value)}
          showIcon
          disabled={calendarActualEndDate}
          style={{ width: "400px", marginTop: "30px", height: "40px" }}
          placeholder={"Gerçeklenen Bitiş Tarihi"}
        />
        <div style={{ width: "400px", marginTop: "30px" }}>
          <Form>
            <Form.Input
              placeholder="Gerçekleşen Efor"
              type="number"
              min="10"
              disabled={inputEffortMade}
              value={effortMade}
              onChange={(e) => setEffortMade(e.target.value)}
            />
          </Form>
        </div>
      </div>
      <div style={{ marginTop: "190px" }}>
        <Form>
          <TextArea
            placeholder="Buraya görev tanımı ekleyin"
            value={taskDescription}
            disabled={textAreaTaskDescription}
            onChange={(e) => setTaskDescription(e.target.value)}
          />
        </Form>
      </div>
      <br />

      <Button
        primary
        disabled={upDateButton}
        onClick={() => {
          handleUpdateTask(task_id);
        }}
      >
        Güncelle
      </Button>

      <br />
      <div style={{ display: "flex" }}>
        <Link to={"/preposttasks"}>
          <Button
            variant="contained"
            style={{ width: "620px", marginRight: "40px", marginTop: "30px" }}
          >
            Öncül veya Ardıl Görev Listesi
          </Button>
        </Link>
        {/* bu görevin id'si ile gitmeli */}
        <Link to={"/taskcomments"}>
          <Button
            variant="contained"
            style={{ width: "620px", marginLeft: "65px", marginTop: "30px" }}
          >
            Görev Yorumları
          </Button>
        </Link>
      </div>
    </div>
  );
}

function splitDateTime(props) {
  const dateArray = props.split("/");
  const year = dateArray[2];
  const month = dateArray[1] - 1; // JavaScript'te ay sayıları 0'dan başlar
  const day = dateArray[0];
  const dateTime = new Date(year, month, day);
  return dateTime;
}
