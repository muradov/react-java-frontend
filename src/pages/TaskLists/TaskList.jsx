import React, { useEffect, useState, useReducer } from "react";
import { Container, Dropdown, Pagination, Table } from "semantic-ui-react";
import SideBar from "../../layouts/SideBar";
import _ from "lodash";
import { useNavigate } from "react-router-dom";
import taskServices from "../../services/taskService/taskServices";

export default function TaskList(props) {

  console.log("props TaskList: ", props);

  const titleList = [
    { key: "0", text: "Tüm görevlerimi getir", value: 0 },
    { key: "1", text: "Proje Yöneticisi", value: 1 },
    { key: "2", text: "Görev Sorumlusu", value: 2 },
    { key: "3", text: "Teknik Lider / Onaylayan", value: 3 },
    { key: "4", text: "Danışman", value: 4 },
    { key: "5", text: "Ürün Sorumlusu", value: 5 },
  ];

  const [role_id, setRole_id] = useState(0);

  const handleDropdownChange = (e, { value }) => {
    setRole_id(value);
  };
  return (
    <div>
      <SideBar />
      <Container className="">
        <Dropdown
          placeholder="Bir rol seçin"
          fluid
          selection
          options={titleList}
          onChange={handleDropdownChange}
        />
        <br />
        <section>
          <SortableTable id={[role_id]} />
        </section>
        <footer style={{ display: "flex", justifyContent: "center" }}>
          <Pagination defaultActivePage={5} totalPages={10} />
        </footer>
      </Container>
    </div>
  );
}

function exampleReducer(state, action) {
  switch (action.type) {
    case "CHANGE_SORT":
      if (state.column === action.column) {
        return {
          ...state,
          data: state.data.slice().reverse(),
          direction:
            state.direction === "ascending" ? "descending" : "ascending",
        };
      } else {
        return {
          column: action.column,
          data: _.sortBy(state.data, [action.column]),
          direction: "ascending",
        };
      }

    case "SET_DATA":
      return { ...state, data: action.payload };

    case "SET_COLUMN":
      return {
        ...state,
        column: action.payload.column,
        direction: action.payload.direction,
      };

    default:
      throw new Error("Invalid action type.");
  }
}

function SortableTable(props) {
  const role_id = props.id[0];

  const [state, dispatch] = useReducer(exampleReducer, {
    column: null,
    data: [],
    direction: null,
  });

  useEffect(() => {
    const fetchData = async () => {
      try {
        let response;
        let user_id = sessionStorage.getItem("user_id");
        console.log("SortableTable user_id ", user_id);
        if (role_id === 0) {
          response = await taskServices().getTaskById(user_id);
        } else {
          response = await taskServices().getTaskByRoleName(role_id, user_id);
        }

        dispatch({ type: "SET_DATA", payload: response });
      } catch (error) {
        console.error(error);
      }
    };
    fetchData();
  }, [role_id]);

  const { column, data, direction } = state;

  const navigate = useNavigate();

  const handleRowClick = (task_id) => {
    sessionStorage.setItem("task_id", task_id);

    navigate("/taskmanagementtaskId");
  };

  return (
    <Table sortable celled fixed>
      <Table.Header>
        <Table.Row>
          <Table.HeaderCell
            sorted={column === "task_id" ? direction : null}
            onClick={() => dispatch({ type: "CHANGE_SORT", column: "task_id" })}
          >
            Görev ID
          </Table.HeaderCell>
          <Table.HeaderCell
            sorted={column === "task_name" ? direction : null}
            onClick={() =>
              dispatch({ type: "CHANGE_SORT", column: "task_name" })
            }
          >
            Görev Başlığı
          </Table.HeaderCell>
          <Table.HeaderCell
            sorted={column === "planned_end_date" ? direction : null}
            onClick={() =>
              dispatch({ type: "CHANGE_SORT", column: "planned_end_date" })
            }
          >
            Planlanan Bitiş Tarihi
          </Table.HeaderCell>
          <Table.HeaderCell
            sorted={column === "priority_name" ? direction : null}
            onClick={() =>
              dispatch({ type: "CHANGE_SORT", column: "priority_name" })
            }
          >
            Öncelik
          </Table.HeaderCell>
        </Table.Row>
      </Table.Header>
      <Table.Body>
        {data.map(({ task_id, task_name, planned_end_date, priority_name }) => (
          <Table.Row key={task_id} onClick={() => handleRowClick(task_id)}>
            <Table.Cell>{task_id}</Table.Cell>
            <Table.Cell>{task_name}</Table.Cell>
            <Table.Cell>{planned_end_date}</Table.Cell>
            <Table.Cell>{priority_name}</Table.Cell>
          </Table.Row>
        ))}
      </Table.Body>
    </Table>
  );
}
