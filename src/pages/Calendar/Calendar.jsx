import React, { useEffect, useState } from 'react';
import { Providers, ProviderState } from '@microsoft/mgt-element';
import { Agenda, Login } from '@microsoft/mgt-react';

import axios from 'axios';
import { PublicClientApplication } from '@azure/msal-browser';

import SideBar from '../../layouts/SideBar';
import { msalConfig, calendarAPI, loginRequest } from "../../utils/authConfig"


const Calendar = () => {
    const [isSignedIn] = useIsSignedIn();
    const [events, setEvents] = useState([]);

    const msalInstance = new PublicClientApplication(msalConfig);

    const getToken = async () => {
        try {
            const accounts = msalInstance.getAllAccounts();

            if (accounts.length === 0) {
                // No signed-in accounts found, initiate the sign-in process
                await msalInstance.loginPopup();
            }

            if (accounts.length > 0) {
                msalInstance.setActiveAccount(accounts[0]);
            }

            const tokenResponse = await msalInstance.acquireTokenSilent({
                loginRequest, // Specify the required scopes for your API requests
                account: msalInstance.getActiveAccount(), // Get the active account for token acquisition
            });

            const accessToken = tokenResponse.accessToken;

            // Make authenticated API requests using Axios
            const apiEndpoint = calendarAPI.getMyCalendar

            const config = {
                headers: {
                    Authorization: `Bearer ${accessToken}`,
                },
            };

            const apiResponse = await axios.get(apiEndpoint, config);

            const data = apiResponse.data;

            console.log("data: ", data); // Do something with the API response data

            setEvents(data.value); // Set the fetched events in the state
        } catch (error) {
            console.log(error);
        }
    };

    useEffect(() => {
        const fetchData = async () => {
            await getToken(); // Call the getToken function to retrieve the access token and make API requests
        };

        fetchData();
    }, []);

    // Render your events in the component
    return (
        <div>
            <SideBar />
            {events.map((event) => (
                <div key={event.id}>
                    <h3>{event.subject}</h3>
                    <p>{event.bodyPreview}</p>
                    {/* Render other event details */}
                </div>
            ))}
            <div>
                {isSignedIn &&
                    <Agenda />}
            </div>
        </div>
    );
};

function useIsSignedIn() {
    const [isSignedIn, setIsSignedIn] = useState(false);

    useEffect(() => {
        const updateState = () => {
            const provider = Providers.globalProvider;
            setIsSignedIn(provider && provider.state === ProviderState.SignedIn);
        };

        Providers.onProviderUpdated(updateState);
        updateState();

        return () => {
            Providers.removeProviderUpdatedListener(updateState);
        }
    }, []);

    return [isSignedIn];
}
export default Calendar;
