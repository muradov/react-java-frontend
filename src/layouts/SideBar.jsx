import React from "react";
import { Link } from "react-router-dom";
import { Icon, Image, Menu, Sidebar } from "semantic-ui-react";
import { Providers } from '@microsoft/mgt-element';
import { useMsal } from "@azure/msal-react";

export default function SideBar() {

  // sessionStorage.removeItem("user_id"); // Remove user_id from session storage

  // const msalProvider = Providers.globalProvider;
  // if (msalProvider) {
  //   msalProvider.logout(); // Sign out from Microsoft account
  //   window.location.href = "/"; // Navigate to the "/" route
  // }

  const { instance } = useMsal();

  const handleLogout = (logoutType) => {
    if (logoutType === "popup") {
      instance.logoutPopup({
        postLogoutRedirectUri: "http://localhost:3000/",
        mainWindowRedirectUri: "http://localhost:3000/",
      });
    } else if (logoutType === "redirect") {
      instance.logoutRedirect({
        postLogoutRedirectUri: "http://localhost:3000/",
      });
    }
  };



  return (
    <Sidebar
      as={Menu}
      animation="overlay"
      icon="labeled"
      inverted
      vertical
      visible
      width="thin"
    >
      <Menu.Item as="a">
        <Link to={"/tasklist"}>
          <Image src={require("../images/proopslogo.png")} />
        </Link>
      </Menu.Item>

      <Menu.Item as="a">
        <Icon name="tasks" />
        <Link to={"/tasklist"}>Görev Listesi</Link>
      </Menu.Item>

      <Menu.Item as="a">
        <Icon name="plus" fluid size="large" />
        Proje Oluşturma
      </Menu.Item>

      <Menu.Item as="a">
        <Icon name="plus" />
        <Link to={"/taskmanagement"}>Görev Oluşturma</Link>
      </Menu.Item>

      <Menu.Item as="a">
        <Icon name="plus" />
        <Link to={"/calendar"}>Takvim</Link>
      </Menu.Item>

      <Menu.Item as="a" style={{ marginTop: 300 }}>
        <Link to={"/"} onClick={() => handleLogout("redirect")}>Çıkış Yap</Link>
      </Menu.Item>
    </Sidebar>
  );
}
