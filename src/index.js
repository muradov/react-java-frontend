import React from "react";
import ReactDOM from "react-dom/client";
import App from "./App";
import reportWebVitals from "./reportWebVitals";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import TaskList from "./pages/TaskLists/TaskList";
import TaskComments from "./pages/TaskComments/TaskComments";
import PrePostTasks from "./pages/PrePostTasks/PrePostTasks";
import TaskManagementWithTaskId from "./pages/TaskManagements/TaskManagementWithTaskId";
import TaskManagement from "./pages/TaskManagements/TaskManagement";
import Calendar from "./pages/Calendar/Calendar";
import { PublicClientApplication } from '@azure/msal-browser';
import { MsalProvider } from '@azure/msal-react';
import { msalConfig } from './utils/authConfig';

const msalInstance = new PublicClientApplication(msalConfig);

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <BrowserRouter>
    <React.StrictMode>
      <MsalProvider instance={msalInstance}>
        <Routes>
          <Route path="/" element={<App />} />
          <Route path="/tasklist" element={<TaskList />} />
          <Route path="/taskcomments" element={<TaskComments />} />
          <Route path="/preposttasks" element={<PrePostTasks />} />
          <Route path="/taskmanagementtaskId" element={<TaskManagementWithTaskId />} />
          <Route path="/taskmanagement" element={<TaskManagement />} />
          <Route path="/calendar" element={<Calendar />} />
        </Routes>
      </MsalProvider>
    </React.StrictMode>
  </BrowserRouter>
);

reportWebVitals();
