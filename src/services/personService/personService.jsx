import axios from "axios";

export default function personService() {
    const REACT_APP_GET_ALL_PERSON = process.env.REACT_APP_GET_ALL_PERSON
    const REACT_APP_GET_ALL_PY_PERSON = process.env.REACT_APP_GET_ALL_PY_PERSON

    return {
        getAllPerson: () => {
            return axios
                .get(REACT_APP_GET_ALL_PERSON)
                .then((response) => {
                    return response.data;
                })
                .catch((error) => {
                    throw error;
                });
        },

        getAllPyPerson: () => {
            return axios
                .get(REACT_APP_GET_ALL_PY_PERSON)
                .then((response) => {
                    return response.data;
                })
                .catch((error) => {
                    throw error;
                });
        },
    };
}
