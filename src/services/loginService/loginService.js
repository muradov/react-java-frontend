import axios from "axios";

export default function loginService() {

  const REACT_APP_LOGIN = process.env.REACT_APP_LOGIN

  return {
    postLogin: (data) => {
      return axios
        .post(REACT_APP_LOGIN, data)
        .then((response) => {
          return response.data;
        })
        .catch((error) => {
          throw error;
        });
    },
  };
}
