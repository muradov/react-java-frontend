import axios from "axios";

export default function commentService() {
  const REACT_APP_GET_COMMENT_BY_ID = process.env.REACT_APP_GET_COMMENT_BY_ID

  return {
    getComments: (id) => {
      return axios
        .get(REACT_APP_GET_COMMENT_BY_ID + id)
        .then((response) => {
          return response.data;
        })
        .catch((error) => {
          throw error;
        });
    },
  };
}
