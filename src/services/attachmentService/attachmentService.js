import axios from "axios";

export default function attachmentService() {

  const REACT_APP_GET_COMMENT_WITH_ATTACHMENTS = process.env.REACT_APP_GET_COMMENT_WITH_ATTACHMENTS

  return {
    getCommentWithAttachments: () => {
      return axios
        .get(REACT_APP_GET_COMMENT_WITH_ATTACHMENTS)
        .then((response) => {
          return response.data;
        })
        .catch((error) => {
          throw error;
        });
    },

    getCommentById: (id) => {
      const url = `${REACT_APP_GET_COMMENT_WITH_ATTACHMENTS}/${id}`;
      return axios
        .get(url)
        .then((response) => {
          return response.data;
        })
        .catch((error) => {
          throw error;
        });
    },
  };
}
