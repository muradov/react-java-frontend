import axios from "axios";

export default function taskServices() {
  const REACT_APP_GET_ALL_PROJECT = process.env.REACT_APP_GET_ALL_PROJECT

  return {
    getAllProject: () => {
      return axios
        .get(REACT_APP_GET_ALL_PROJECT)
        .then((response) => {
          return response.data;
        })
        .catch((error) => {
          throw error;
        });
    },
  };
}
