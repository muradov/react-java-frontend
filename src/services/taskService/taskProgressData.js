import axios from "axios";

export default function taskProgressDataService() {
    const REACT_APP_GET_PROGRESS_DATA = process.env.REACT_APP_GET_PROGRESS_DATA
    return {
        getAllProgress: () => {
            return axios
                .get(REACT_APP_GET_PROGRESS_DATA)
                .then((response) => {
                    return response.data;
                })
                .catch((error) => {
                    throw error;
                });
        },
    };
}
