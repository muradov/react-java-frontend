import axios from "axios";

export default function taskSituationDataService() {
    const REACT_APP_GET_SITUATION_DATA = process.env.REACT_APP_GET_SITUATION_DATA
    return {
        getAllSituation: () => {
            return axios
                .get(REACT_APP_GET_SITUATION_DATA)
                .then((response) => {
                    return response.data;
                })
                .catch((error) => {
                    throw error;
                });
        },
    };
}
