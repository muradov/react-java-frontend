import axios from "axios";

export default function taskServices() {
  // const API_URL = "http://localhost:8080/api/getTaskById?id=";
  // const API_URL_GET_TASK_BY_ID="http://localhost:8080/api/getTaskByRoleName?"

  const REACT_APP_GET_TASK_BY_ID = process.env.REACT_APP_GET_TASK_BY_ID
  const REACT_APP_GET_TASK_BY_ROLE_NAME = process.env.REACT_APP_GET_TASK_BY_ROLE_NAME
  return {

    getAllTasks: () => {
      return axios
        .get(REACT_APP_GET_TASK_BY_ID)
        .then((response) => {
          return response.data;
        })
        .catch((error) => {
          throw error;
        });
    },

    getTaskById: (id) => {
      const url = `${REACT_APP_GET_TASK_BY_ID}${id}`;
      console.log("url:", url);
      return axios
        .get(url)
        .then((response) => {
          return response.data;
        })
        .catch((error) => {
          throw error;
        });
    },


    getTaskByRoleName: (role_id, id) => {
      const url = `${REACT_APP_GET_TASK_BY_ROLE_NAME}role_id=${role_id}&id=${id}`
      console.log("url:", url);
      return axios
        .get(url)
        .then((response) => {
          return response.data;
        })
        .catch((error) => {
          throw error;
        });
    },

  };
}
