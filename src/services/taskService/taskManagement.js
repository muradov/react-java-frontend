import axios from "axios";

export default function taskManagement() {

  const API_URL_GET_ROLE_ID_BY_TASK_ID_AND_PERSON_ID = process.env.REACT_APP_GET_ROLE_ID_BY_TASK_ID_AND_PERSON_ID
  const API_URL_GET_ADD_TASK = process.env.REACT_APP_GET_ADD_TASK
  const API_URL_GET_BY_ID_TASK_MANAGEMENT = process.env.REACT_APP_GET_BY_ID_TASK_MANAGEMENT
  const API_URL_GET_TASK_BY_ID_EFFORT = process.env.REACT_APP_GET_TASK_BY_ID_EFFORT
  const API_URL_GET_UPDATE_TASKS = process.env.REACT_APP_GET_UPDATE_TASKS

  return {
    addTask: (data) => {
      console.log("data:", data);
      return axios
        .post(API_URL_GET_ADD_TASK, data)
        .then((response) => {
          return response.data;
        })
        .catch((error) => {
          throw error;
        });
    },
    getTask: (id) => {
      console.log(id);
      return axios
        .get(API_URL_GET_BY_ID_TASK_MANAGEMENT + id)
        .then((response) => {
          console.log(response.data);
          return response.data;
        })
        .catch((error) => {
          throw error;
        });
    },

    getTaskByIdEffort: (id) => {
      console.log(id);
      return axios
        .get(API_URL_GET_TASK_BY_ID_EFFORT + id)
        .then((response) => {
          return response.data;
        })
        .catch((error) => {
          throw error;
        });
    },

    getRoleIdByTaskIdAndPersonId: (task_id, person_id) => {
      const URL = `${API_URL_GET_ROLE_ID_BY_TASK_ID_AND_PERSON_ID}task_id=${task_id}&person_id=${person_id}`;
      return axios
        .get(URL)
        .then((response) => {
          return response.data;
        })
        .catch((error) => {

          throw error;
        });
    },

    updateTask: (task_id, data) => {
      const URL_UPDATE_TASK = API_URL_GET_UPDATE_TASKS + task_id;

      return axios
        .put(URL_UPDATE_TASK, data)
        .then((response) => {
          console.log(response);
          return response.data;
        })
        .catch((error) => {
          console.log(error);
          throw error;
        });
    },
  };
}
