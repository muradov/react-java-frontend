import axios from "axios";

export default function taskPriorityDataService() {
  const REACT_APP_GET_PRIORITY_DATA = process.env.REACT_APP_GET_PRIORITY_DATA
  return {
    getAllPriority: () => {
      return axios
        .get(REACT_APP_GET_PRIORITY_DATA)
        .then((response) => {
          return response.data;
        })
        .catch((error) => {
          throw error;
        });
    },
  };
}
