import axios from "axios";

export default function taskDataServices() {
  const REACT_APP_GET_TYPE_DATA = process.env.REACT_APP_GET_TYPE_DATA
  return {
    getAllTaskType: () => {
      return axios
        .get(REACT_APP_GET_TYPE_DATA)
        .then((response) => {
          return response.data;
        })
        .catch((error) => {
          throw error;
        });
    },
  };
}
